#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool comparar ( char *, char * ); //ver nota 1
char * leerCadena (char *);
int main(int argc, char* argv[]) {

/* Suponemos cadenas de máximo 20 caracteres, por lo cual necesitamos
 vectores de char de 21 posiciones: 20 para los caracteres y uno para
 el carácter nulo del final. C no protege contra el desborde en los
 vectores: si se teclean más de 20 caracteres, los que sobren
 sobrescribirán otras variables en memoria con resultados impredecibles
*/

 char * v[100]; //ver nota 2
 char s[21]; //ver nota 3
 int i, total, numCadenas,j = 0;

 printf("Cuantas cadenas desea teclear ? ");
 scanf("%d", &numCadenas);	

 while (numCadenas > 100 || numCadenas <= 0)
 {
 	printf("Error, ingrese un numero de cadenas en el rango de 1 a 100 \n");
 	printf("Cuantas cadenas desea teclear ? ");
 	scanf("%d", &numCadenas);
 }

 for ( i = 0; i < numCadenas;  i++){ 
 	char* cadenaAux;
 	cadenaAux = (char*) malloc(21); 		
 	printf( "Teclee cadena %d e ingrese el indice donde desea guardarla en el arreglo seguido de una coma: ", i ); //ver nota 6
 	scanf( "%s,%d", cadenaAux, &j); //ver nota 7
 	
 	while (j < 0 || j > 101)
 	{
 		printf("Error, ingrese un indice valido entre el rango de 1 a 100 \n");
 		scanf("%d", &j);
 	}

 	v[j] = (char *) malloc( 21 ); //ver notas 4 y 5  
 	v[j] = cadenaAux;
 	free(cadenaAux);
 }

printf( "Teclee cadena para buscar:" );
scanf( "%s", s ); //ver nota 8

for ( i = 0; i < 5; i++){
	if ( strcmp( s, v[i] ) ){
		total++;
	}
}
printf( "La cadena %s esta %d veces en el vector\n", s, total );

//ver nota 9
 return 0; //ver nota 10
}

char * leerCadena (void)
{
	char cdAux2[21];
	char caracter = getchar();	

	for (int u = 0; u < 21 && caracter != '\n'; u++)
	{
		cdAux2[u] = caracter;
		caracter = getchar();		
	}
	char * enlace = &cdAux2[0];
	return enlace;
}

/*
bool comparar ( char * s, char * t ) {
 //Mientras que la primera cadena no se acabe
 while ( *s != '\0' ){ //ver nota 11
 if ( *s != *t )
 return false; //Si difieren en algún carácter, son diferentes
 else{
 s++; //ver nota 12
 t++;
 }
 }
 //La primera cadena ya se acabó ...
 if ( *t != '\0' )
 return false; //... si la segunda no se ha acabado, son diferentes
 else
 return true;
}
*/

